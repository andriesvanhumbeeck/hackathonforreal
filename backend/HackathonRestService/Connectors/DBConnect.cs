﻿using HackathonRestService.Properties;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace HackathonRestService.Connectors
{
    public class DBConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public DBConnect()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = Settings.Default.Server;
            database = Settings.Default.Database;
            uid = Settings.Default.Uid;
            password = Settings.Default.Password;
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (MySqlException ex)
                {

                    switch (ex.Number)
                    {
                        case 0:
                            Trace.TraceError("Cannot connect to server.  Contact administrator");
                            break;

                        case 1045:
                            Trace.TraceError("Invalid username/password, please try again");
                            break;
                    }
                    return false;
                }
            }
            else
                return true;
                
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Trace.TraceError(ex.Message);
                return false;
            }
        }

        public MySqlDataReader ReadQuery(string query)
        {
            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                try
                {
                    return cmd.ExecuteReader();
                }
                catch (Exception ex)
                {

                    Trace.TraceError(ex.Message);
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        public int CountQuery(string query)
        {
            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                try
                {
                    return int.Parse(cmd.ExecuteScalar()+"");
                }
                catch (Exception ex)
                {

                    Trace.TraceError(ex.Message);
                }

                return 0;
            }
            else
            {
                return 0;
            }
        }

        public void WriteQuery(string query)
        {
            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    
                    Trace.TraceError(ex.Message);
                }

                //close connection
                this.CloseConnection();
            }
        }
        
    }
}