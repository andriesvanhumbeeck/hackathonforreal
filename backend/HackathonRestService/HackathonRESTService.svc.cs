﻿using HackathonRestService.Connectors;
using HackathonRestService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace HackathonRestService
{
    
    public class HackathonRESTServiceImpl : HackathonRESTService
    {

        private DBConnect dbConnection;

        public HackathonRESTServiceImpl()
        {
            dbConnection = new DBConnect();
        }
        public HalteInfo GetHalteInfo()
        {
            return new HalteInfo
            {
                Naam ="India", Lat=64.478,Lng =64.478, Gemeente="sachin.jpg"
            };
         }

        public string RegisterForTeam(string devID, string teamID)
        {
            return "Registered " + devID +" successfully for team "+ teamID;
        }

        public Team GetTeam(string deviceId)
        {
            return Team.getTeam(dbConnection, deviceId);
        }

        public List<Team> GetTeams()
        {
            return Team.GetTeams(dbConnection);
        }

        public JSONMessage RegisterCapture(string userID, string devID, string teamID, string beaconID, string major, string minor)
        {
            if (BusStop.checkLastCaptured(dbConnection, userID,beaconID,major,minor) == 0)
            {
                if (BusStop.registerCapture(dbConnection, userID, devID, teamID, beaconID, major, minor) == 0)
                    return new JSONMessage { message = "Bus Stop captured!" };
                else
                    return new JSONMessage { message = "Error capturing Bus Stop!" };
            }

            else
            {
                return new JSONMessage { message = "You already captured this bus stop in the last hour!" };
            }
            
        }

        public ScoreList GetScores(string beaconID, string major, string minor)
        {
            return ScoreList.getScores(dbConnection,beaconID, major, minor);
        }
    }
}
