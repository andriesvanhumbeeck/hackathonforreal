﻿using HackathonRestService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace HackathonRestService
{
    [ServiceContract]
    public interface HackathonRESTService
    {

        [WebGet(UriTemplate = "/GethalteInfo",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare)]
        HalteInfo GetHalteInfo();

        [WebGet(UriTemplate = "/RegisterForTeam/device/{devID}/team/{teamID}",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare)]
        string RegisterForTeam(string devID, string teamID);

        [WebGet(UriTemplate = "/getTeam/device/{devID}",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare)]
        Team GetTeam(string devID);

        [WebGet(UriTemplate = "/getTeams",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare)]
        List<Team> GetTeams();

        [WebGet(UriTemplate = "/RegisterCapture/user/{userID}/device/{devID}/team/{teamID}/beacon/{beaconID}/major/{major}/minor/{minor}",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare)]
        JSONMessage RegisterCapture(string userID, string devID,string teamID, string beaconID, string major, string minor);

        [WebGet(UriTemplate = "/getScores/beacon/{beaconID}/major/{major}/minor/{minor}",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare)]
        ScoreList GetScores(string beaconID, string major, string minor);

    }

}
