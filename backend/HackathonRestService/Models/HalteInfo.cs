﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace HackathonRestService.Models
{
    [DataContract]
    public class HalteInfo
    {
        [DataMember]
        public string Naam { get; set; }
        [DataMember]
        public double Lat { get; set; }
        [DataMember]
        public double Lng { get; set; }
        [DataMember]
        public string Gemeente { get; set; }


    }
}
