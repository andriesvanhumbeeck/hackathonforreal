﻿using HackathonRestService.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace HackathonRestService.Models
{
    [DataContract]
    public class ScoreList
    {
        [DataMember]
        public List<Team> teams;
        [DataMember]
        public BusStop busStop;

        public static ScoreList getScores(DBConnect connection,string beaconID, string major, string minor)
        {
            var connectionString = "SELECT t.idTeam,t.Name AS TeamName,s.Score,b.Name AS StopName from BusStop b " +
                                            "join StopTeamLinker s on s.idStop = b.idStop " +
                                            "join Team t on t.idTeam = s.idTeam " +
                                            "where b.BeaconId ='" + beaconID + major + minor+"'";
            Console.WriteLine(connectionString);
            var data = connection.ReadQuery(connectionString);
             if (data != null)
             {
                 List<Team> team = new List<Team>();
                 BusStop stop = new BusStop();
                 while (data.Read())
                 {
                     int idTeam = (int)data["idTeam"];
                     string TeamName = (string)data["TeamName"];
                     int score = (int)data["Score"];
                     team.Add(new Team { id = idTeam, Name = TeamName, Score=score });
                     stop.Name = (string)data["StopName"];

                 }
                 data.Close();
                 connection.CloseConnection();
                 return new ScoreList { teams = team, busStop = stop };
             }
             else
             {
                 connection.CloseConnection();
                 return new ScoreList { teams = null, busStop = null };
             }
                 
        }
    }
}