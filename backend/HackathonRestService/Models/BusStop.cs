﻿using HackathonRestService.Connectors;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace HackathonRestService.Models
{
    [DataContract]
    public class BusStop
    {
        [DataMember]
        public string Name;
        public static int registerCapture(DBConnect connection, string userid, string devID, string teamID, string beaconID, string major, string minor)
        {
            try
            {
                //Step1: insert user-deviceid link
                connection.WriteQuery("INSERT INTO Device (idUser, idDevice) SELECT * FROM (SELECT '" + userid + "','" + devID + "') AS tmp WHERE NOT EXISTS ( SELECT idDevice FROM Device WHERE idDevice = '" + devID + "' ) LIMIT 1;");
                //Step2: Set user's last stop and teamID
                connection.WriteQuery("UPDATE User SET idStop = (SELECT idStop from BusStop where BeaconId ='" + beaconID + major + minor + "' LIMIT 1),idTeam=" + teamID + ",CaptureDate='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' where idUser=" + userid);
                //Step3: Set team - busstop linking if it doesn't exist
                connection.WriteQuery("INSERT INTO StopTeamLinker (idStop, idTeam,Score) SELECT * FROM (SELECT (SELECT idStop from BusStop where BeaconId ='" + beaconID + major + minor + "' LIMIT 1), '" + teamID + "','0') AS tmp WHERE NOT EXISTS ( SELECT idStop FROM StopTeamLinker WHERE idStop = (SELECT idStop from BusStop where BeaconId ='" + beaconID + major + minor + "' LIMIT 1) AND idTeam = '" + teamID + "' ) LIMIT 1;");
                //Step4: Set new Score
                connection.WriteQuery("UPDATE StopTeamLinker SET Score=Score+1 WHERE idStop = (SELECT idStop from BusStop where BeaconId ='" + beaconID + major + minor + "' LIMIT 1) AND idTeam = '" + teamID + "'");

                return 0;
            }
            catch (Exception e)
            {

                Trace.TraceError(e.Message);
                return -1;
            }

        }

        public static int checkLastCaptured(DBConnect connection, string userid, string beaconID, string major, string minor)
        {

            var query1 = "SELECT COUNT(*) FROM User u JOIN BusStop b ON u.idStop = b.idStop WHERE b.BeaconId='"+beaconID+major+minor+"'";
            var isLastCaptured = connection.CountQuery(query1);

            if(isLastCaptured == 1)
            {
                var query2 = "SELECT u.CaptureDate FROM User u JOIN BusStop b ON u.idStop = b.idStop WHERE u.idUser =" + userid + " LIMIT 1";
                //return connection.ReadQuery(query);
                var data = connection.ReadQuery(query2);
                int returnValue = 0;
                while (data.Read())
                {
                    DateTime captureDate = (DateTime)data["CaptureDate"];
                    if (captureDate > DateTime.Now.AddSeconds(-10))
                    {
                        return 1;
                    }

                }
                data.Close();
                connection.CloseConnection();
                return returnValue;
            }
            else
            {
                return 0;
            }
            
        }
    }
}