﻿using HackathonRestService.Connectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace HackathonRestService.Models
{
    [DataContract]
    public class Team
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Score { get; set; }
        
        public static Team getTeam(DBConnect connection,string idDevice)
        {
            var data = connection.ReadQuery("SELECT t.idTeam,t.Name from team t "+
                                            "join user u on t.idTeam = u.idTeam " +
                                            "join device d on u.idUser = d.idUser " +
                                            "where d.idDevice ='"+idDevice+"'");
            if(data != null)
            {
                List<Team> team = new List<Team>();
                while (data.Read())
                {
                    int idTeam = (int)data["idTeam"];
                    string TeamName = (string)data["Name"];
                    team.Add(new Team { id = idTeam, Name = TeamName });

                }
                data.Close();
                connection.CloseConnection();

                if(team.Count() == 0)
                {
                    return new Team { id = -1, Name = "No Team" };
                }

                else if(team.Count() == 1)
                {
                    return team.ElementAt(0);
                }
                else{

                    Trace.TraceError("Multiple teams found!");
                    return new Team{id = -1, Name = "No Team"};
                }
            }
            else
            {
                connection.CloseConnection();
                Trace.TraceError("Error fetching database data!");
                return new Team { id = -1, Name = "No Team" };
            }
            
        }

        public static void RegisterForTeam(DBConnect connection, string idDevice, string idTeam)
        {
            connection.WriteQuery("INSERT INTO User (idDevice,idTeam) VALUES ('" + idDevice + "','" + idTeam + "')");
        }

        public static List<Team> GetTeams(DBConnect connection)
        {
             var data = connection.ReadQuery("SELECT * from team");
             List<Team> team = new List<Team>();
             if (data != null)
             {
                 
                 while (data.Read())
                 {
                     int idTeam = (int)data["idTeam"];
                     string TeamName = (string)data["Name"];
                     team.Add(new Team { id = idTeam, Name = TeamName });

                 }
                 data.Close();

             }
             connection.CloseConnection();

             return team;

        }
    }
}