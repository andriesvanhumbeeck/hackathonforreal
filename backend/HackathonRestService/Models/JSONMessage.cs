﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace HackathonRestService.Models
{
    [DataContract]
    public class JSONMessage
    {
        [DataMember]
        public string message;

    }
}