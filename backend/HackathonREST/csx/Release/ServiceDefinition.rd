﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="HackathonREST" generation="1" functional="0" release="0" Id="273ad6a3-402f-4983-b1fd-e996cedb9e4e" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="HackathonRESTGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="HackathonRestService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/HackathonREST/HackathonRESTGroup/LB:HackathonRestService:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="HackathonRestService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/HackathonREST/HackathonRESTGroup/MapHackathonRestService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="HackathonRestServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/HackathonREST/HackathonRESTGroup/MapHackathonRestServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:HackathonRestService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/HackathonREST/HackathonRESTGroup/HackathonRestService/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapHackathonRestService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/HackathonREST/HackathonRESTGroup/HackathonRestService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapHackathonRestServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/HackathonREST/HackathonRESTGroup/HackathonRestServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="HackathonRestService" generation="1" functional="0" release="0" software="C:\Users\Yannick.Geerts\documents\visual studio 2013\Projects\DeLijnTest\HackathonREST\csx\Release\roles\HackathonRestService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;HackathonRestService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;HackathonRestService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/HackathonREST/HackathonRESTGroup/HackathonRestServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/HackathonREST/HackathonRESTGroup/HackathonRestServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/HackathonREST/HackathonRESTGroup/HackathonRestServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="HackathonRestServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="HackathonRestServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="HackathonRestServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="9feea265-99bc-4ab8-a186-057475aed450" ref="Microsoft.RedDog.Contract\ServiceContract\HackathonRESTContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="d86fa0d9-9093-45c1-b20f-2a8669c2fb35" ref="Microsoft.RedDog.Contract\Interface\HackathonRestService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/HackathonREST/HackathonRESTGroup/HackathonRestService:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>