var baseurl = "http://aehackathon.cloudapp.net/HackathonRESTService.svc";
var device_id = 'no_id';
var teamnr;
var beaconRegion;
var lastHitMillis = 0;

function createBeacon() {
    var uuid = 'b9407f30-f5f8-466e-aff9-25556b57fe6d'; // mandatory
    var identifier = 'Estimote Beacon'; // mandatory
    var minor = 39826; // 39826 optional, defaults to wildcard if left empty
    var major = 196; // 196 optional, defaults to wildcard if left empty

    // throws an error if the parameters are not valid
    var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid); // , major, minor)

    return beaconRegion;   
} 

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    checkIfTeam: function() {
        // console.log('checkIfTeam');
        $.ajax({
            type: 'GET',
            url: baseurl+'/getTeam/device/'+device_id,
        }).done(function(data) {
            var teamid = data['id'];
            // console.log("teamnummer: "+ teamid);
            app.setTitle(teamid);
            teamnr = teamid;

            if (nmr != 1 && nmr != 2) {
                $('#chooseteampageid').click();
            }
        }).fail(function(er) {
            console.log("checkifteam error");
        });
    },

    registerForTeam: function(team) {
        $.ajax({
            type: 'GET',
            url: baseurl+'/RegisterForTeam/device/'+device_id+'/team/'+team,
        }).done(function(data) {
            console.log(data);
            
        }).fail(function(er) {
            console.log(er);
            
        });
    },

    setTitle: function(nummer) {
        var title = document.getElementById("team_name");
        if (nummer == -1) {
            // show #chooseteampage
            title.innerHTML = "NO TEAM"
        } else if (nummer == 1) {
            // show #
            title.innerHTML = "Team RED";
        } else if (nummer == 2) {
            title.innerHTML = "Team BLUE";
        } else {
            title.innerHTML = "Team GO";
        }
    },

    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // console.log("ONDEVICEREADY");
        app.receivedEvent('deviceready');

        device_id = device.uuid;
        // app.checkIfTeam();
        console.log(device_id);
        // console.log("DEVICE_ID: " + device.uuid);

        var delegate = new cordova.plugins.locationManager.Delegate().implement({

            didDetermineStateForRegion: function (pluginResult) {

                // logToDom('[DOM] didDetermineStateForRegion: ' + JSON.stringify(pluginResult));

                // cordova.plugins.locationManager.appendToDeviceLog('[DOM] didDetermineStateForRegion: '
                //     + JSON.stringify(pluginResult));

                // navigator.notification.alert(
                //     'You LEFT a Region!',  // message
                //     function() {},         // callback
                //     'Game Over',            // title
                //     'Done'                  // buttonName
                // );
            },

            didStartMonitoringForRegion: function (pluginResult) {
                console.log('didStartMonitoringForRegion:', pluginResult);

                logToDom('didStartMonitoringForRegion:' + JSON.stringify(pluginResult));

                // navigator.notification.alert(
                //     'You LEFT a Region!',  // message
                //     function() {},         // callback
                //     'Game Over',            // title
                //     'Done'                  // buttonName
                // );
            },
            

            didRangeBeaconsInRegion: function (pluginResult) {
            //    console.log('didRangeBeaconsInRegion ' + JSON.stringify(pluginResult));
            //    var item = document.getElementById('halte_id');
            //    item.innerText = "";
            //    for (var i = 0; i < pluginResult.beacons.length; i++) {
            //         item.innerText += pluginResult.beacons[0].major + "-" + pluginResult.beacons[0].minor;
            //    }
        
                //var item = document.getElementById('logdiv');
                //item.innerText = JSON.stringify(pluginResult);
                //cordova.plugins.locationManager.stopRangingBeaconsInRegion(beaconRegion);

                // logToDom('[DOM] didRangeBeaconsInRegion: ' + JSON.stringify(pluginResult));
            },

            didEnterRegion: function(pluginResults) {
                // Android / BlackBerry OS 5 - 7 and BlackBerry 10 / iOS / Tizen

                // navigator.notification.alert(
                //     'You Entered a Region!',  // message
                //     function() {},         // callback
                //     'Game Over',            // title
                //     'Done'                  // buttonName
                // );
                var TIMEOUT_MILLIS = 1000 * 10;
                if ((lastHitMillis  == 0) || (lastHitMillis + TIMEOUT_MILLIS) < new Date().getTime())
                {
                    lastHitMillis = new Date().getTime();

                    var item = document.getElementById('logdiv');
                    item.innerHTML = JSON.stringify(pluginResults) + " - " + lastHitMillis;
                    
                    
                    //cordova.plugins.locationManager.stopRangingBeaconsInRegion(beaconRegion);
                }
                
            },

            didExitRegion: function(pluginResults) {
                // navigator.notification.alert(
                //     'You LEFT a Region!',  // message
                //     function() {},         // callback
                //     'Game Over',            // title
                //     'Done'                  // buttonName
                // );
            }

        });

    cordova.plugins.locationManager.setDelegate(delegate);
    beaconRegion = createBeacon();
    //cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion);
    cordova.plugins.locationManager.startMonitoringForRegion(beaconRegion);
    //     .fail(console.error)
    //     .done();
        



    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // var parentElement = document.getElementById(id);
        // var listeningElement = parentElement.querySelector('.listening');
        // var receivedElement = parentElement.querySelector('.received');

        // listeningElement.setAttribute('style', 'display:none;');
        // receivedElement.setAttribute('style', 'display:block;');

        // debug log
        console.log('Received Event: ' + id);
    }

};
            
