$(function () {
    $('#hccontainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: 'Heverlee<br/>Station',
            align: 'center',
            verticalAlign: 'middle',
            y: 50
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        series: [{
            type: 'pie',
            name: 'Heverlee Station',
            innerSize: '50%',
            data: [
                {
                    name: 'Team Red',
                    y: 40,
                    color: 'red'
                },
                {
                    name: 'Team Blue',
                    y: 60,
                    color: 'blue'
                }

            ]
        }]
    });
});

