var baseurl = "http://aehackathon.cloudapp.net/HackathonRESTService.svc";
var teamnr;
var device_id = "no_id";

var indexapp = {

    init: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    checkIfTeam: function() {
        // TODO: disable both buttons
        
        navigator.notification.activityStart("Fetching Team data", "Fetching..");
        $.ajax({
            type: 'GET',
            url: baseurl+'/getTeam/device/'+device_id,
        }).done(function(data) {
            var teamid = data['id'];
            // console.log("teamnummer: "+ teamid);
            teamnr = teamid;
            console.log(teamnr);

            if (teamnr == 1) { // notification + button for redirect (in callback van notification!!)
                // navigator.notification.alert("Your Team: RED", function(){
                //     // window.location.href="halte.html?team=red";
                // }, "Team Found", "Continue");
                $('#choosered').removeClass('disabled');
                $('.glyph-red').css('display', 'inline');
            } else if (teamnr == 2) { // notification + button for redirect (in callback van notification!!)
                // navigator.notification.alert("Your Team: BLUE", function(){
                //     // window.location.href="halte.html?team=blue";
                // }, "Team Found", "Continue");
                $('#chooseblue').removeClass('disabled');
                $('.glyph-blue').css('display', 'inline');
            } else { // if no team, enable all buttons
                navigator.notification.alert("Your device ID: "+device_id, function(){}, "No Team Found", "Continue");
                $('#choosered').removeClass('disabled');
                $('#chooseblue').removeClass('disabled');
            }
            
            
        }).fail(function(er) {
            console.log("checkifteam error");
            navigator.notification.alert("Device ID: "+device_id+"\nError: "+er, function(){ }, "Server Error", "Continue");
        }).always(function() {
            navigator.notification.activityStop();
        });

    },

    onDeviceReady: function() {
        device_id = device.uuid;
        $('#choosered').addClass('disabled');
        $('#chooseblue').addClass('disabled');
        $('.glyphicon').css('display', 'none');
        indexapp.checkIfTeam();
       
    }

};

indexapp.init();