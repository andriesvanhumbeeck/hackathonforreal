var baseurl = "http://aehackathon.cloudapp.net/HackathonRESTService.svc";
// http://aehackathon.cloudapp.net/HackathonRESTService.svc/getScores/beacon/b9407f30-f5f8-466e-aff9-25556b57fe6d/major/39826/minor/196
var device_id = 'no_id';
var Globaluuid = 'b9407f30-f5f8-466e-aff9-25556b57fe6d';
var teamnr;
var url;

var majorMonitored;
var minorMonitored;

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

function createBeacon() {
    var uuid = 'b9407f30-f5f8-466e-aff9-25556b57fe6d'; // mandatory
    var identifier = 'Estimote Beacon'; // mandatory
    var major; // 39826 optional, defaults to wildcard if left empty
    var minor; // 196 optional, defaults to wildcard if left empty

    // throws an error if the parameters are not valid
    var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid); // , major, minor)

    return beaconRegion;
}

function createBeacon2(major, minor) {
    var uuid = Globaluuid; // mandatory
    var identifier = 'Estimote Beacon'; // mandatory

    // throws an error if the parameters are not valid
    var beaconRegion;
    if (major == null && minor == null) {
        console.log("no minor or major");
        beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid);
    } else if (major != null && minor != null) {
        console.log("minor AND major");
        beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, major, minor);
    } else {
        console.log("other");
        beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid);
    }
    return beaconRegion;
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    checkIfTeam: function() {
        // console.log('checkIfTeam');
        $.ajax({
            type: 'GET',
            url: baseurl+'/getTeam/device/'+device_id,
        }).done(function(data) {
            var teamid = data['id'];
            // console.log("teamnummer: "+ teamid);
            teamnr = teamid;
            console.log(teamnr);

            if (nmr != 1 && nmr != 2) {
                $('#chooseteampageid').click();
            }
        }).fail(function(er) {
            console.log("checkifteam error");
        });
    },

    // registerForTeam: function(team) {
    //     $.ajax({
    //         type: 'GET',
    //         url: baseurl+'/RegisterForTeam/device/'+device_id+'/team/'+team,
    //     }).done(function(data) {
    //         // console.log(data);
    //         // update html elementen om scores weer te geven
            
    //     }).fail(function(er) {
    //         console.log(er);
    //     });
    // },

    /**
    * /getScores/beacon/{beaconID}/major/{major}/minor/{minor}
    */
    setHalteData: function(beaconID, major, minor) {
        var el = document.getElementById("score_red_id");
        var el2 = document.getElementById("score_blue_id");
        var el3 = document.getElementById("halte_naam_id");
        url = baseurl+'/getScores/beacon/'+beaconID+'/major/'+major+'/minor/'+minor;
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: url,
        }).done(function(data) {
            console.log("SUCCESS: "+data);
            var teamsX = data.teams;
            for (var i = 0; i < data.teams.length; i++) {
                var bluescore, redscore;
                if(teamsX[i].Name == 'Red') {
                    redscore = teamsX[i].Score;
                    el.innerHTML = redscore;
                } else if (teamsX[i].Name == 'Blue') {
                    bluescore = teamsX[i].Score;
                    el2.innerHTML = bluescore;
                }
                if (bluescore < redscore) {
                    el.innerHTML += '  <span class="glyphicon glyphicon-star"></span>';
                } else if (bluescore > redscore) {
                    el2.innerHTML += '  <span class="glyphicon glyphicon-star"></span>';
                } else {
                    // no image
                }
            }
            el3.innerHTML = data.busStop.Name;

        }).fail(function(er) {
            console.log("ERROR: "+er);
            console.log(url);
        }).always(function() {
            // enable button
            $('#capture_button_id').removeClass("disabled");
        });
    },

    registerCapture: function(userId, devId, teamId, beaconId, major, minor) {
        navigator.notification.activityStart();
        url = baseurl+'/RegisterCapture/user/'+userId+'/device/'+devId+'/team/'+teamId+'/beacon/'+beaconId+'/major/'+major+'/minor/'+minor;
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: url,
        }).done(function(data) {
            console.log("SUCCESS: "+JSON.stringify(data));
            // data.message
            navigator.notification.alert(data.message, function(){
                app.setHalteData(beaconId, major, minor);
            }, "Server Response", "Continue");

        }).fail(function(er) {
            console.log("ERROR: "+er);
            navigator.notification.alert(er, function(){ }, "Server Error", "Continue");
        }).always(function() {
            navigator.notification.activityStop();
        });
    },


    resetHalteData: function() {
        var el = document.getElementById("score_red_id");
        var el2 = document.getElementById("score_blue_id");
        var el3 = document.getElementById("halte_naam_id");
        el.innerHTML = "Geen halte";
        el2.innerHTML = "Geen halte";
        el3.innerHTML = "Geen halte";

        // disable button
        $('#capture_button_id').addClass("disabled");
    },

    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        device_id = device.uuid;
        // app.checkIfTeam();
        // console.log(device_id);
        // console.log("DEVICE_ID: " + device.uuid);

        var delegate = new cordova.plugins.locationManager.Delegate().implement({

            didDetermineStateForRegion: function (pluginResult) {

                // logToDom('[DOM] didDetermineStateForRegion: ' + JSON.stringify(pluginResult));

                // cordova.plugins.locationManager.appendToDeviceLog('[DOM] didDetermineStateForRegion: '
                //     + JSON.stringify(pluginResult));

            },

            didStartMonitoringForRegion: function (pluginResult) {
                console.log('didStartMonitoringForRegion:', pluginResult);
                var mj = pluginResults.region.major;
                var mn = pluginResults.region.minor;

            },
            

            didRangeBeaconsInRegion: function (pluginResult) {
                console.log('didRangeBeaconsInRegion ' + JSON.stringify(pluginResult));
                var item = document.getElementById('halte_id');
                item.innerText = "";

                for (var i = 0; i < pluginResult.beacons.length - 1; i++) {
                     // item.innerText += pluginResult.beacons[0].major + "-" + pluginResult.beacons[0].minor + " ";
                    item.innerText += " O -";
                }
                item.innerText += " O";

                if (pluginResult.beacons.length == 0) {
                    item.innerText = "";
                }
  

            },

            didEnterRegion: function(pluginResults) {
                // Android / BlackBerry OS 5 - 7 and BlackBerry 10 / iOS / Tizen
                var el3 = document.getElementById("halte_naam_id");
                var mj = pluginResults.region.major;
                var mn = pluginResults.region.minor;
                minorMonitored = mn;
                majorMonitored = mj;

                app.setHalteData('b9407f30-f5f8-466e-aff9-25556b57fe6d',mj,mn);
                // $('#buttonpanel').css('border-width', '3px');
                // navigator.notification.alert(
                //     'You ENTERED a Region!',  // message
                //     function() {},         // callback
                //     'Alert',            // title
                //     'Continue'                  // buttonName
                // );
                
            },

            didExitRegion: function(pluginResults) {
                // TODO: set halte op 0 wanneer terug uit range
                app.resetHalteData();
                $('#buttonpanel').css('border-width', '1px');
            }

        });

    cordova.plugins.locationManager.setDelegate(delegate);
    cordova.plugins.locationManager.startRangingBeaconsInRegion(createBeacon());

    // cordova.plugins.locationManager.startMonitoringForRegion(createBeacon2(39826,196))
    //     .fail(console.error)
    //     .done();
    cordova.plugins.locationManager.startMonitoringForRegion(createBeacon2(57959,41724))
        .fail(console.error)
        .done();

    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        // debug log
        console.log('Received Event: ' + id);
    }

};
            