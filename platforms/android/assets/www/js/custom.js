$(document).ready(function(){

    $('.lineHeightDiv').each(function(){
        var div_height = $(this).parent('div').height();
        $(this).css('lineHeight', (div_height ) + "px");
    });

    $(window).on("orientationchange",function(){
	  	$('.lineHeightDiv').each(function(){
        	var div_height = $(this).parent('div').height();
	        $(this).css('lineHeight', (div_height ) + "px");
	    });
	});
});