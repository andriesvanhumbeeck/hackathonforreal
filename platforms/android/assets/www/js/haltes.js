function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}


$(document).ready(function(){

	var teamcolor = getQueryVariable('team');
	var teamnumber;
	var el = document.getElementById("addteamcolor");
	var el2 = document.getElementById("addteamtitle");
	var btn1 = document.getElementById("capture_button_id");
    if (teamcolor == "red") {
    	teamnumber = 1;
    	$('#capture_button_id').addClass('btn-danger');
    	$('#capture_button_id').removeClass('btn-default');
    	$('#buttonpanel').css('border', 'solid 1px #D9534F');
    	if(el) {
		    el.className += el.className ? ' team-red' : 'team-red';
		}
		if(el2) {
		    el2.className += el2.className ? ' team-title-red' : 'team-title-red';
		    el2.innerHTML = "Team Red";
		}
    } else if (teamcolor == "blue") {
    	teamnumber = 2;
    	$('#capture_button_id').addClass('btn-info');
    	$('#capture_button_id').removeClass('btn-default');
    	$('#buttonpanel').css('border', 'solid 1px #5BC0DE');
    	if(el) {
		    el.className += el.className ? ' team-blue' : 'team-blue';
		}
		if(el2) {
		    el2.className += el2.className ? ' team-title-blue' : 'team-title-blue';
		    el2.innerHTML = "Team Blue";
		}
    }

    $('#capture_button_id').on('click', function() {
        app.registerCapture(1,device_id,teamnumber,'b9407f30-f5f8-466e-aff9-25556b57fe6d',majorMonitored,minorMonitored);
    });

});
